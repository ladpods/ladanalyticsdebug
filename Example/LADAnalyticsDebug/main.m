//
//  main.m
//  LADAnalyticsDebug
//
//  Created by laddev on 10/21/2016.
//  Copyright (c) 2016 laddev. All rights reserved.
//

@import UIKit;
#import "LADAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LADAppDelegate class]));
    }
}
