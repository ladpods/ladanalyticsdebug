//
//  LADViewController.m
//  LADAnalyticsDebug
//
//  Created by laddev on 10/21/2016.
//  Copyright (c) 2016 laddev. All rights reserved.
//

#import "LADViewController.h"

//
#import <LADAnalyticsDebug/LADAnalyticsDebug.h>

// ---------------------------------------------------------------------------------

@interface LADViewController ()

@property (nonatomic, strong) NSArray *configurations;

@end

// ---------------------------------------------------------------------------------

@implementation LADViewController

// ---------------------------------------------------------------------------------
#pragma mark - View Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.configurations = @[[LADAnalyticsAdConfigurationInfos configurationInfosWithAdName:@"TEST" adIdentifier:@"ID"], [LADAnalyticsAdConfigurationInfos configurationInfosWithAdName:@"TEST" adIdentifier:@"ID"], [LADAnalyticsAdConfigurationInfos configurationInfosWithAdName:@"TEST" adIdentifier:@"ID"], [LADAnalyticsAdConfigurationInfos configurationInfosWithAdName:@"TEST" adIdentifier:@"ID"]];
}

// ---------------------------------------------------------------------------------

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [LADAnalyticsDebug startWithServices:LADAnalyticsGoogleService];
    [LADAnalyticsDebug addAdConfigurations:self.configurations];
    
    [NSTimer scheduledTimerWithTimeInterval:2.
                                     target:self selector:@selector(__increment) userInfo:nil repeats: YES];
}

// ---------------------------------------------------------------------------------
#pragma mark - Memory Methods

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// ---------------------------------------------------------------------------------
#pragma mark - Test Methods
- (void)__increment
{
    if ([LADAnalyticsDebug isActive])
    {
        NSDictionary *servicesInfosComplement = [[NSMutableDictionary alloc] init];
        [servicesInfosComplement setValue:@"1" forKey:@"siteID"];
        [servicesInfosComplement setValue:@"TestView Screen" forKey:@"screen"];
        [servicesInfosComplement setValue:@"Hello world" forKey:@"title"];
        
        NSString *getRandomStr  = [self randomStringWithLength:106];
        
        [servicesInfosComplement setValue:[getRandomStr copy] forKey:@"random string"];
        
        LADAnalyticsServiceInfos *serviceInfo = [LADAnalyticsServiceInfos infosWithService:LADAnalyticsGoogleService
                                                                                complement:[servicesInfosComplement debugDescription]];
        [LADAnalyticsDebug incrementServiceWithInfos:serviceInfo];
    }
}


- (NSString *) randomStringWithLength: (int) len
{
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}


@end
