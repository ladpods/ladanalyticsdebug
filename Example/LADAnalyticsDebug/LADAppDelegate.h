//
//  LADAppDelegate.h
//  LADAnalyticsDebug
//
//  Created by laddev on 10/21/2016.
//  Copyright (c) 2016 laddev. All rights reserved.
//

@import UIKit;

@interface LADAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
