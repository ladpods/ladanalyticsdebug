# LADAnalyticsDebug 

Analytics services debugger:

- Increment / Decrement counter based on a specific analytics service
- Display, filter & send logs for a specific analytics service
- Display ad configurations.

## Summary

- [Installation](#installation)
- [Usage](#usage)
- [LADAnalyticsService](#ladanalyticsservice)
- [LADAnalyticsServiceInfos](#ladanalyticsserviceinfos)
- [LADAnalyticsAdConfigurationInfos](#ladanalyticsadconfigurationinfos)
- [Examples](#examples)
- [Author](#author)
- [License](#license)

## Installation

```ruby
pod 'LADAnalyticsDebug'
```

## Usage

The start and stop methods are based on a specific deeplink scheme. To test if a deeplink can match:
```objc
+ (BOOL)isAnalyticsDebugDeeplink:(NSString *)deeplink;
```
**The deeplink format must be: appname://analyticsdebug/...**

Handle the analytics debug deeplink scheme:
```objc
+ (void)handleDeeplink:(NSString *)deeplink
enableCompletion:(void (^) ())enableCompletion
disableCompletion:(void (^) ())disableCompletion;
```
**To execute the enable completion block, the deeplink format must be: appname://analyticsdebug/enable and appname://analyticsdebug/disable to exectute the disable completion block**

Start the analytics debug. It will present a debug popup with a counter for all choosen [LADAnalyticsService](#ladanalyticsservice):
```objc
+ (void)startWithServices:(LADAnalyticsService)services;
```

Stop the analytics debug. It will remove the current debug popup:
```objc
+ (void)stop;
```

Increment a counter for choosen [LADAnalyticsService](#ladanalyticsservice) and add a [LADAnalyticsServiceInfos](#ladanalyticsserviceinfos) object to the current infos stack to display complement details:
```objc
+ (void)incrementServiceWithInfos:(LADAnalyticsServiceInfos *)infos;
```

Add app [LADAnalyticsAdConfigurationInfos](#ladanalyticsadconfigurationinfos) (Inter, banner, DFP, ...):
```objc
+ (void)addAdConfigurations:(NSArray<LADAnalyticsAdConfigurationInfos *> *)adConfigurations;
```

Replace the default decrement interval value (default is 3.0 sec):
```objc
+ (void)setDecrementIntervalValue:(NSTimeInterval)decrementIntervalValue;
```

##### LADAnalyticsService

Represents the different analytics services.
```objc
typedef NS_OPTIONS(NSUInteger, LADAnalyticsService) {
LADAnalyticsUnknownService      = 0,

LADAnalyticsLocalyticsService   = 1 << 0,   // Localytics
LADAnalyticsATInternetService   = 1 << 1,   // ATInternet
LADAnalyticsWysistatService     = 1 << 2,   // Wysistat
LADAnalyticsGoogleService       = 1 << 3,   // Google analytics
LADAnalyticsAdInterService      = 1 << 4,   // Ad inter
LADAnalyticsAdBannerService     = 1 << 5,   // Ad banner
LADAnalyticsKruxService         = 1 << 6,   // Krux Analytics

LADAnalyticsAllService          = 1 << 7
};
```

##### LADAnalyticsServiceInfos

Represents complement informations for a specific analytics service.
Use in the details controller to display all the added LADAnalyticsServiceInfos.
```objc
+ (instancetype)infosWithService:(LADAnalyticsService)service
complement:(NSString *)complement;
```

The [LADAnalyticsService](#ladanalyticsservice):
```objc
@property (nonatomic, assign, readonly) LADAnalyticsService service;
```

The analytics service name:
```objc
@property (nonatomic, copy, readonly) NSString *serviceName;
```

The analytics service complement infos:
```objc
@property (nonatomic, copy, readonly) NSString *complement;
```

The analytics service date. [NSDate date] when object is added:
```objc
@property (nonatomic, strong, readonly) NSDate *date;
```

##### LADAnalyticsAdConfigurationInfos

Represents configuration informations for a specific ad. 
Use in the details controller to display ad configurations.
```objc
+ (instancetype)configurationInfosWithAdName:(NSString *)adName
adIdentifier:(NSString *)adIdentifier;
```

The ad name:
```objc
@property (nonatomic, copy, readonly) NSString *adName;
```

The ad identifier:
```objc
@property (nonatomic, copy, readonly) NSString *adIdentifier;
```

## Examples

- Start and stop LADAnalyticsDebug service:

```objc
if ([LADAnalyticsDebug isAnalyticsDebugDeeplink:url.absoluteString]) {
[LADAnalyticsDebug handleDeeplink:url.absoluteString
enableCompletion:^{
[LADAnalyticsDebug startWithServices:LADAnalyticsAllService];
}
disableCompletion:^{
[LADAnalyticsDebug stop];
}];
}
```

- Increment a specific service:

```objc
[LADAnalyticsDebug incrementServiceWithInfos:[LADAnalyticsServiceInfos infosWithService:LADAnalyticsLocalyticsService complement:@"Screen_Name"]];
```

- Add ad configuration:

```objc
[LADAnalyticsDebug addAdConfigurations:@[[LADAnalyticsAdConfigurationInfos configurationInfosWithAdName:@"Ad_Name" adIdentifier:@"Ad_Identifier"]]];
```

- Update decrement interval value:

```objc
[LADAnalyticsDebug setDecrementIntervalValue:5.];
```

## Author

laddev, jean-baptiste.castro@lagardere-active.com

## License

LADAnalyticsDebug is available under the MIT license. See the LICENSE file for more info.
