#import "LADAnalyticsDebugDetailsCell.h"

// Category
#import "NSBundle+LADAnalyticsDebug.h"

// ---------------------------------------------------------------------------------

static NSDateFormatter *_dateFormatter = nil;

// ---------------------------------------------------------------------------------
#pragma mark - Constants

static NSString * const kPopupCellReuseIdentifier = @"LADAnalyticsDebugDetailsCell";

// ---------------------------------------------------------------------------------
#pragma mark - Interface

@interface LADAnalyticsDebugDetailsCell (/* Private */)

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *complementLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;

@end

// ---------------------------------------------------------------------------------
#pragma mark - Implementation


@implementation LADAnalyticsDebugDetailsCell


// ---------------------------------------------------------------------------------
#pragma mark - Property

+ (NSDateFormatter *)dateFormatter
{
    if (!_dateFormatter)
    {
        _dateFormatter            = [NSDateFormatter new];
        _dateFormatter.dateFormat = @"HH:mm";
    }
    
    return _dateFormatter;
}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];
    

}

// ---------------------------------------------------------------------------------
#pragma mark - Public

+ (void)registerInCollectionView:(UICollectionView *)collectionView
{
    NSBundle *bundle = [NSBundle lad_bundleFromClass:[LADAnalyticsDebugDetailsCell class]];
    
    [collectionView registerNib:[UINib nibWithNibName:kPopupCellReuseIdentifier bundle:bundle]
     forCellWithReuseIdentifier:kPopupCellReuseIdentifier];
}

+ (NSString *)identifier {
    return kPopupCellReuseIdentifier;
}

// ---------------------------------------------------------------------------------

- (void)configureWithName:(NSString *)name
               complement:(NSString *)complement
                     date:(NSDate *)date{
    
    self.nameLabel.text       = name;
    self.complementLabel.text = complement;
    self.dateLabel.text       = [[LADAnalyticsDebugDetailsCell dateFormatter] stringFromDate:date];
}

// ---------------------------------------------------------------------------------

+ (CGFloat)heightForCell:(LADAnalyticsDebugDetailsCell *)cell {
    CGSize calculatedSize = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingExpandedSize];
    return calculatedSize.height;
}

@end
