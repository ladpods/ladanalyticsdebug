#import <UIKit/UIKit.h>


@interface LADAnalyticsDebugDetailsCell : UICollectionViewCell


+ (void)registerInCollectionView:(UICollectionView *)collectionView;
+ (NSString *)identifier;

- (void)configureWithName:(NSString *)name
               complement:(NSString *)complement
                     date:(NSDate *)date;

+ (CGFloat)heightForCell:(LADAnalyticsDebugDetailsCell *)cell;




@end
