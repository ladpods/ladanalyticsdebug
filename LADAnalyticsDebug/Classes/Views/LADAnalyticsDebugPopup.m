#import "LADAnalyticsDebugPopup.h"

// Model
#import "LADAnalyticsDebugPopupService.h"

// View
#import "LADAnalyticsDebugPopupCell.h"


#pragma mark - Constants


static const CGFloat kAnimationDuration = .25;


#pragma mark - Interface


@interface LADAnalyticsDebugPopup (/* Private */) <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate>


@property (nonatomic, strong) NSMutableArray *services;

@property (nonatomic, strong) UICollectionView *collectionView;

// Setup
- (void)__setupContainer;
- (void)__setupServices:(LADAnalyticsService)services;
- (void)__setupCollectionView;

// Action
- (void)__didTappedAction;

// Utils
- (LADAnalyticsDebugPopupService *)__serviceWithShortName:(NSString *)shortName;
- (void)__decrement:(NSTimer *)timer;


@end


#pragma mark - Implementation


@implementation LADAnalyticsDebugPopup


#pragma mark - Initialization


+ (instancetype)popupWithServices:(LADAnalyticsService)services {
    return [[self alloc] initWithServices:services];
}


- (instancetype)initWithServices:(LADAnalyticsService)services {
    self = [super init];
    if (self) {
        [self __setupContainer];
        [self __setupServices:services];
        [self __setupCollectionView];
    }
    
    return self;
}


#pragma mark - Constraints


- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.collectionView.frame = self.bounds;
}


#pragma mark - Public


- (void)incrementService:(LADAnalyticsService)service {
    LADAnalyticsDebugPopupService *popupService = [self __serviceWithShortName:LADAnalyticsShortNameFromService(service)];
    
    if (popupService != nil)
    {
        [popupService incrementCounter];
        
        [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:popupService.index inSection:0]]];
        
        [NSTimer scheduledTimerWithTimeInterval:self.decrementInterval
                                         target:self
                                       selector:@selector(__decrement:)
                                       userInfo:@{@"shortName": popupService.shortName}
                                        repeats:NO];
    }
    else
    {
        NSLog(@"!!! Error : can't increment for service call (not configure): %@",LADAnalyticsNameFromService(service));
    }
}


- (void)show {
    self.hidden = NO;
    
    [UIView animateWithDuration:kAnimationDuration
                     animations:^{
                         self.alpha = 1;
                     }];
}


- (void)hide{
    [UIView animateWithDuration:kAnimationDuration
                     animations:^{
                         self.alpha = 0;
                     } completion:^(BOOL finished) {
                         self.hidden = YES;
                     }];
}


#pragma mark - UICollectionViewDelegateFlowLayout


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(CGRectGetWidth(collectionView.bounds) / [self.services count], CGRectGetHeight(collectionView.bounds));
}


#pragma mark - UICollectionViewDataSource


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.services count];
}


- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LADAnalyticsDebugPopupService *popupService = self.services[indexPath.row];
    
    return [LADAnalyticsDebugPopupCell cellForCollectionView:collectionView
                                                 atIndexPath:indexPath
                                                        name:popupService.shortName
                                                       value:popupService.counterValue];
}


#pragma mark - Setup


- (void)__setupContainer {
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(0, .5);
    self.layer.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.30].CGColor;
    self.layer.shadowOpacity = 0.5;
    self.layer.shadowRadius = 1.5;
    
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(__didTappedAction)]];
}


- (void)__setupServices:(LADAnalyticsService)services {
    if (LADAnalyticsUnknownService == services) {
        return;
    }
    
    if (LADAnalyticsAllService == services) {
        [self __setupServices:(LADAnalyticsLocalyticsService|LADAnalyticsATInternetService|LADAnalyticsWysistatService|LADAnalyticsGoogleService|LADAnalyticsAdInterService|LADAnalyticsAdBannerService | LADAnalyticsKruxService)];
    }
    else {
        self.services = [NSMutableArray new];
        
        if (services & LADAnalyticsLocalyticsService) {
            [self.services addObject:[LADAnalyticsDebugPopupService serviceWithShortName:LADAnalyticsShortNameFromService(LADAnalyticsLocalyticsService) index:self.services.count]];
        }
        
        if (services & LADAnalyticsATInternetService) {
            [self.services addObject:[LADAnalyticsDebugPopupService serviceWithShortName:LADAnalyticsShortNameFromService(LADAnalyticsATInternetService) index:self.services.count]];
        }
        
        if (services & LADAnalyticsWysistatService) {
            [self.services addObject:[LADAnalyticsDebugPopupService serviceWithShortName:LADAnalyticsShortNameFromService(LADAnalyticsWysistatService) index:self.services.count]];
        }
        
        if (services & LADAnalyticsGoogleService) {
            [self.services addObject:[LADAnalyticsDebugPopupService serviceWithShortName:LADAnalyticsShortNameFromService(LADAnalyticsGoogleService) index:self.services.count]];
        }
        
        if (services & LADAnalyticsAdInterService) {
            [self.services addObject:[LADAnalyticsDebugPopupService serviceWithShortName:LADAnalyticsShortNameFromService(LADAnalyticsAdInterService) index:self.services.count]];
        }
        
        if (services & LADAnalyticsAdBannerService) {
            [self.services addObject:[LADAnalyticsDebugPopupService serviceWithShortName:LADAnalyticsShortNameFromService(LADAnalyticsAdBannerService) index:self.services.count]];
        }
        
        if (services & LADAnalyticsKruxService) {
            [self.services addObject:[LADAnalyticsDebugPopupService serviceWithShortName:LADAnalyticsShortNameFromService(LADAnalyticsKruxService) index:self.services.count]];
        }
    }
}


- (void)__setupCollectionView {
    UICollectionViewFlowLayout *flowLayout = [UICollectionViewFlowLayout new];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:flowLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.4];
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addSubview:self.collectionView];
    
    // Register
    [LADAnalyticsDebugPopupCell registerInCollectionView:self.collectionView];
}


#pragma mark - Action


- (void)__didTappedAction {
    if (self.onTouchAction) {
        self.onTouchAction();
    }
}


#pragma mark - Utils


- (LADAnalyticsDebugPopupService *)__serviceWithShortName:(NSString *)shortName {
    if (!shortName ||
        !shortName.length) {
        
        return nil;
    }
    
    for (LADAnalyticsDebugPopupService *service in self.services) {
        if ([shortName isEqualToString:service.shortName]) {
            return service;
        }
    }
    
    return nil;
}


- (void)__decrement:(NSTimer *)timer {
    
    NSString *shortName = timer.userInfo[@"shortName"];
    LADAnalyticsDebugPopupService *popupService = [self __serviceWithShortName:shortName];
    
    if (!popupService ||
        [popupService.counterValue integerValue] == 0) {
        
        return;
    }
    
    [popupService decrementCounter];
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:popupService.index inSection:0]]];
    [CATransaction commit];
}


@end
