#import <UIKit/UIKit.h>

//
#import "LADAnalyticsDebug.h"


@interface LADAnalyticsDebugPopup : UIView


/*!
 * @discussion Initialize a popup with analytics services.
 * @param services The analytics service to debug.
 */

+ (instancetype)popupWithServices:(LADAnalyticsService)services;


@property (nonatomic, assign) NSTimeInterval decrementInterval;     // Interval use to decrement services counter.
@property (nonatomic, copy) void(^onTouchAction)(void);             // Callback completion when popup is touched.


/*!
 * @discussion Increment the popup counter for choosen analytics services. It will decrement after a decrementInterval.
 * @param service An analytics service to increment.
 */

- (void)incrementService:(LADAnalyticsService)service;

- (void)show;
- (void)hide;


@end
