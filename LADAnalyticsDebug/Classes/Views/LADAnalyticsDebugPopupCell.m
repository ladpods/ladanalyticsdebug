#import "LADAnalyticsDebugPopupCell.h"

// Category
#import "NSBundle+LADAnalyticsDebug.h"


#pragma mark - Constants


static NSString * const kPopupCellReuseIdentifier = @"LADAnalyticsDebugPopupCell";


#pragma mark - Interface


@interface LADAnalyticsDebugPopupCell (/* Private */)


@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *valueLabel;


@end


#pragma mark - Implementation


@implementation LADAnalyticsDebugPopupCell


#pragma mark - Public


+ (void)registerInCollectionView:(UICollectionView *)collectionView {
    NSBundle *bundle = [NSBundle lad_bundleFromClass:[LADAnalyticsDebugPopupCell class]];
    
    [collectionView registerNib:[UINib nibWithNibName:kPopupCellReuseIdentifier bundle:bundle]
     forCellWithReuseIdentifier:kPopupCellReuseIdentifier];
}


+ (LADAnalyticsDebugPopupCell *)cellForCollectionView:(UICollectionView *)collectionView
                                          atIndexPath:(NSIndexPath *)indexPath
                                                 name:(NSString *)name
                                                value:(NSNumber *)value {
    
    LADAnalyticsDebugPopupCell *cell = (LADAnalyticsDebugPopupCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kPopupCellReuseIdentifier
                                                                                                               forIndexPath:indexPath];
    cell.selectedBackgroundView = [UIView new];
    cell.nameLabel.text = name;
    cell.valueLabel.text = [value stringValue];
    
    return cell;
}


@end
