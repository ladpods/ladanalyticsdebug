#import <UIKit/UIKit.h>


@interface LADAnalyticsDebugPopupCell : UICollectionViewCell


+ (void)registerInCollectionView:(UICollectionView *)collectionView;
+ (LADAnalyticsDebugPopupCell *)cellForCollectionView:(UICollectionView *)collectionView
                                          atIndexPath:(NSIndexPath *)indexPath
                                                 name:(NSString *)name
                                                value:(NSNumber *)value;


@end
