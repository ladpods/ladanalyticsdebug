#import <Foundation/Foundation.h>


@interface NSBundle (LADAnalyticsDebug)


+ (NSBundle *)lad_bundleFromClass:(Class)aClass;


@end
