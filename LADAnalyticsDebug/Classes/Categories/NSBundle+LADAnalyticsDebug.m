#import "NSBundle+LADAnalyticsDebug.h"


@implementation NSBundle (LADAnalyticsDebug)


+ (NSBundle *)lad_bundleFromClass:(Class)aClass {
    NSBundle *podBundle = [NSBundle bundleForClass:aClass];
    NSURL *bundleURL = [podBundle URLForResource:@"LADAnalyticsDebug"
                                   withExtension:@"bundle"];
    
    return [NSBundle bundleWithURL:bundleURL];
}


@end
