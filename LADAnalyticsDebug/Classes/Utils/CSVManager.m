#import "CSVManager.h"


@implementation CSVManager


+ (NSData *)CSVFromString:(NSString *)string {
    return [string dataUsingEncoding:NSUTF8StringEncoding];
}


+ (NSData *)CSVAtPath:(NSString *)path {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path]) {
        return nil;
    }
    
    return [fileManager contentsAtPath:path];
}


+ (BOOL)saveCSV:(NSData *)CSV
         atPath:(NSString *)path {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path]) {
        return [fileManager createFileAtPath:path
                                    contents:CSV
                                  attributes:nil];
    }
    
    return FALSE;
}


+ (void)removeCSVAtPath:(NSString *)path{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path]) {
        return;
    }
    
    NSError *error;
    if (![fileManager removeItemAtPath:path
                                 error:&error]) {
        NSLog(@"Error: %@", error);
    }
}


+ (NSString *)CSVMimeType {
    return @"text/csv";
}


@end
