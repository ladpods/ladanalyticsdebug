#import <Foundation/Foundation.h>


@interface CSVManager : NSObject


+ (NSData *)CSVFromString:(NSString *)string;

+ (NSData *)CSVAtPath:(NSString *)path;

+ (BOOL)saveCSV:(NSData *)CSV
         atPath:(NSString *)path;
+ (void)removeCSVAtPath:(NSString *)path;

+ (NSString *)CSVMimeType;


@end
