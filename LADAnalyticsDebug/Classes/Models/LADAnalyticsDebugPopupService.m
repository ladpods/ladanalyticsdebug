#import "LADAnalyticsDebugPopupService.h"


#pragma mark - Interface


@interface LADAnalyticsDebugPopupService ()


@property (nonatomic, strong) NSNumber *counterValue;


@end


#pragma mark - Implementation


@implementation LADAnalyticsDebugPopupService


#pragma mark - Initialization


+ (instancetype)serviceWithShortName:(NSString *)shortName
                               index:(NSUInteger)index {
    
    return [[self alloc] initWithShortName:shortName
                                     index:index];
}


- (instancetype)initWithShortName:(NSString *)shortName
                            index:(NSUInteger)index {
    
    self = [super init];
    if (self) {
        _shortName = shortName;
        _index = index;
        
        _counterValue = @(0);
    }
    
    return self;
}


#pragma mark - Public


- (void)incrementCounter {
    self.counterValue = [NSNumber numberWithInteger:([self.counterValue integerValue] + 1)];
}


- (void)decrementCounter{
    NSInteger value = [self.counterValue integerValue];
    if (value == 0) {
        return;
    }
    
    self.counterValue = [NSNumber numberWithInteger:(value - 1)];
}


@end
