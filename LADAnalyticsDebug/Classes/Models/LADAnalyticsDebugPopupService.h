#import <Foundation/Foundation.h>


@interface LADAnalyticsDebugPopupService : NSObject


+ (instancetype)serviceWithShortName:(NSString *)shortName
                               index:(NSUInteger)index;

@property (nonatomic, copy, readonly) NSString *shortName;
@property (nonatomic, assign, readonly) NSUInteger index;

@property (nonatomic, strong, readonly) NSNumber *counterValue;

- (void)incrementCounter;
- (void)decrementCounter;


@end
