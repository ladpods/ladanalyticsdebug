#import <UIKit/UIKit.h>


@class LADAnalyticsDebugPopup;


@interface LADAnalyticsDebugDetailsViewController : UIViewController


+ (instancetype)detailsWithServicesInfosStack:(NSArray *)servicesInfosStack
                             adConfigurations:(NSArray *)adConfigurations;


@property (nonatomic, strong) LADAnalyticsDebugPopup *popup;


@end
