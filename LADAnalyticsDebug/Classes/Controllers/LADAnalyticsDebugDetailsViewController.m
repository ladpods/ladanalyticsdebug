#import "LADAnalyticsDebugDetailsViewController.h"

#import <MessageUI/MFMailComposeViewController.h>

// Category
#import "NSBundle+LADAnalyticsDebug.h"

// Service
#import "LADAnalyticsDebug.h"

// View
#import "LADAnalyticsDebugDetailsCell.h"
#import "LADAnalyticsDebugPopup.h"

// Utils
#import "CSVManager.h"

// ---------------------------------------------------------------------------------

#pragma mark - Interface

@interface LADAnalyticsDebugDetailsViewController () <UICollectionViewDataSource, UICollectionViewDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, strong)               NSArray                     *servicesInfosStack;
@property (nonatomic, strong)               NSMutableArray              *filteredServicesInfosStack;
@property (nonatomic, copy)                 NSArray                     *adConfigurations;

@property (nonatomic, weak)     IBOutlet    UIButton                    *filterButton;
@property (nonatomic, assign)               BOOL                        filterEnabled;

@property (nonatomic, weak)     IBOutlet    UICollectionView            *collectionView;
@property (nonatomic, weak)     IBOutlet    UICollectionViewFlowLayout  *flowLayout;

// ---------------------------------------------------------------------------------

// Customize
- (void)__customizeNavigationBar;

// Actions
- (void)__cancelAction;
- (void)__sendAction;
- (void)__refreshAction;
- (IBAction)__filterAction:(id)sender;
- (void)__filterWithService:(LADAnalyticsService)service;
- (IBAction)__configurationAction:(id)sender;

// Utils
- (BOOL)__saveCSFFiles;
- (void)__removeCSVFiles;

@end

// ---------------------------------------------------------------------------------
#pragma mark - Implementation

@implementation LADAnalyticsDebugDetailsViewController

// ---------------------------------------------------------------------------------
#pragma mark - Initialization


+ (instancetype)detailsWithServicesInfosStack:(NSArray *)servicesInfosStack
                             adConfigurations:(NSArray *)adConfigurations
{
    return [[self alloc] initWithServicesInfosStack:servicesInfosStack
                                   adConfigurations:adConfigurations];
}

// ---------------------------------------------------------------------------------

- (instancetype)initWithServicesInfosStack:(NSArray *)servicesInfosStack
                          adConfigurations:(NSArray *)adConfigurations
{
    NSBundle *bundle = [NSBundle lad_bundleFromClass:[self class]];
    
    self = [super initWithNibName:NSStringFromClass([self class]) bundle:bundle];
    
    if (self)
    {
        _servicesInfosStack = servicesInfosStack;
        _adConfigurations = adConfigurations;
    }
    
    return self;
}

// ---------------------------------------------------------------------------------
#pragma mark - Properties

- (UIBarButtonItem *)cancelItem
{
    return [[UIBarButtonItem alloc] initWithTitle:@"Annuler"
                                            style:UIBarButtonItemStylePlain
                                           target:self
                                           action:@selector(__cancelAction)];
}

// ---------------------------------------------------------------------------------

- (NSArray *)rightItems
{
    UIBarButtonItem *refreshItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                                 target:self
                                                                                 action:@selector(__refreshAction)];
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                              target:nil
                                                                              action:nil];
    UIBarButtonItem *sendItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                              target:self
                                                                              action:@selector(__sendAction)];
    
    return @[refreshItem, flexItem, sendItem];
}

// ---------------------------------------------------------------------------------

- (NSArray *)servicesInfosStack
{
    return [[_servicesInfosStack reverseObjectEnumerator] allObjects];
}

// ---------------------------------------------------------------------------------

- (NSString *)servicesFileName
{
    return @"services.csv";
}

// ---------------------------------------------------------------------------------

- (NSString *)servicesPath
{
    NSString *documentDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES)[0];
    
    return [documentDirectory stringByAppendingPathComponent:[self servicesFileName]];
}

// ---------------------------------------------------------------------------------

- (NSString *)servicesCSVString
{
    NSMutableArray *services = [NSMutableArray arrayWithCapacity:[self.servicesInfosStack count]];
    
    for (LADAnalyticsServiceInfos *servicesInfos in self.servicesInfosStack)
    {
        [services addObject:[NSString stringWithFormat:@"Date: %@ -- Nom: %@ -- Complement: %@", servicesInfos.date, servicesInfos.serviceName, servicesInfos.complement]];
    }
    
    return [services componentsJoinedByString:@",\n"];
}

// ---------------------------------------------------------------------------------

- (NSString *)adConfigurationsFileName
{
    return @"adConfigurations.csv";
}

// ---------------------------------------------------------------------------------

- (NSString *)adConfigurationsPath
{
    NSString *documentDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES)[0];
    
    return [documentDirectory stringByAppendingPathComponent:[self adConfigurationsFileName]];
}

// ---------------------------------------------------------------------------------

- (NSString *)adConfigurationsCSVString
{
    NSMutableArray *configurations = [NSMutableArray arrayWithCapacity:[self.adConfigurations count]];

    for (LADAnalyticsAdConfigurationInfos *configurationInfos in self.adConfigurations)
    {
        [configurations addObject:[NSString stringWithFormat:@"Nom: %@ -- Identifiant: %@", configurationInfos.adName, configurationInfos.adIdentifier]];
    }
    
    return [configurations componentsJoinedByString:@",\n"];
}

// ---------------------------------------------------------------------------------
#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self __customizeNavigationBar];
    
    [LADAnalyticsDebugDetailsCell registerInCollectionView:self.collectionView];
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    //self.flowLayout.estimatedItemSize = CGSizeMake(300, 70);
}

// ---------------------------------------------------------------------------------

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.collectionView reloadData];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    [self.collectionView.collectionViewLayout invalidateLayout];
}

// ---------------------------------------------------------------------------------
#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

// ---------------------------------------------------------------------------------

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return (self.filterEnabled) ? [self.filteredServicesInfosStack count] : [self.servicesInfosStack count];
}

// ---------------------------------------------------------------------------------

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LADAnalyticsServiceInfos *infos = (self.filterEnabled) ?  self.filteredServicesInfosStack[indexPath.row] : self.servicesInfosStack[indexPath.row];
    LADAnalyticsDebugDetailsCell *cell = (LADAnalyticsDebugDetailsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:[LADAnalyticsDebugDetailsCell identifier]
                                                                                                                   forIndexPath:indexPath];
    [cell configureWithName:infos.serviceName
                 complement:infos.complement
                       date:infos.date];
    
    return cell;
}

// ---------------------------------------------------------------------------------


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LADAnalyticsServiceInfos *infos = (self.filterEnabled) ?  self.filteredServicesInfosStack[indexPath.row] : self.servicesInfosStack[indexPath.row];
     NSBundle *bundle = [NSBundle lad_bundleFromClass:[self class]];
    
    LADAnalyticsDebugDetailsCell *cell = (LADAnalyticsDebugDetailsCell *)[[bundle loadNibNamed:NSStringFromClass([LADAnalyticsDebugDetailsCell class]) owner:self options:nil]
                                                                          firstObject];
    [cell configureWithName:infos.serviceName
                 complement:infos.complement
                       date:infos.date];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    return CGSizeMake(CGRectGetWidth(self.collectionView.bounds), [LADAnalyticsDebugDetailsCell heightForCell:cell]);
}



// ---------------------------------------------------------------------------------
#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    __weak __typeof__(self) weakSelf = self;
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 __typeof__(self) strongSelf = weakSelf;
                                 
                                 [strongSelf __removeCSVFiles];
                             }];
}

// ---------------------------------------------------------------------------------
#pragma mark - Customize

- (void)__customizeNavigationBar
{
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.leftBarButtonItem = [self cancelItem];
    self.navigationItem.rightBarButtonItems = [self rightItems];
}

// ---------------------------------------------------------------------------------
#pragma mark - Actions

- (void)__cancelAction
{
    [self.popup show];
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

// ---------------------------------------------------------------------------------

- (void)__sendAction
{
#if TARGET_IPHONE_SIMULATOR
    return;
#else
    
    MFMailComposeViewController *mailComposeViewController = [MFMailComposeViewController new];
    mailComposeViewController.mailComposeDelegate = self;
    [mailComposeViewController setSubject:@"[LADAnalyticsDebug] - Services & Ad configurations"];
    
    if ([self __saveCSFFiles])
    {
        // Add services CSV
        [mailComposeViewController addAttachmentData:[CSVManager CSVAtPath:[self servicesPath]]
                                            mimeType:[CSVManager CSVMimeType]
                                            fileName:[self servicesFileName]];
        // Add ad configurations CSV
        [mailComposeViewController addAttachmentData:[CSVManager CSVAtPath:[self adConfigurationsPath]]
                                            mimeType:[CSVManager CSVMimeType]
                                            fileName:[self adConfigurationsFileName]];
        
        [self presentViewController:mailComposeViewController
                           animated:YES
                         completion:nil];
    }
#endif
}

// ---------------------------------------------------------------------------------

- (void)__refreshAction
{
    [self.collectionView reloadData];
}

// ---------------------------------------------------------------------------------

- (IBAction)__filterAction:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Filtrer par"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction *allFilterAction = [UIAlertAction actionWithTitle:@"Tous" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [self __filterWithService:LADAnalyticsAllService];
    }];
    
    UIAlertAction *llFilterAction = [UIAlertAction actionWithTitle:LADAnalyticsNameFromService(LADAnalyticsLocalyticsService) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [self __filterWithService:LADAnalyticsLocalyticsService];
    }];
    
    UIAlertAction *atFilterAction = [UIAlertAction actionWithTitle:LADAnalyticsNameFromService(LADAnalyticsATInternetService) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [self __filterWithService:LADAnalyticsATInternetService];
    }];
    
    UIAlertAction *wyFilterAction = [UIAlertAction actionWithTitle:LADAnalyticsNameFromService(LADAnalyticsWysistatService) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [self __filterWithService:LADAnalyticsWysistatService];
    }];
    
    UIAlertAction *goFilterAction = [UIAlertAction actionWithTitle:LADAnalyticsNameFromService(LADAnalyticsGoogleService) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [self __filterWithService:LADAnalyticsGoogleService];
    }];
    
    UIAlertAction *inFilterAction = [UIAlertAction actionWithTitle:LADAnalyticsNameFromService(LADAnalyticsAdInterService) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [self __filterWithService:LADAnalyticsAdInterService];
    }];
    
    UIAlertAction *baFilterAction = [UIAlertAction actionWithTitle:LADAnalyticsNameFromService(LADAnalyticsAdBannerService) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [self __filterWithService:LADAnalyticsAdBannerService];
    }];
    
    UIAlertAction *kruxFilterAction = [UIAlertAction actionWithTitle:LADAnalyticsNameFromService(LADAnalyticsKruxService) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [self __filterWithService:LADAnalyticsKruxService];
    }];
    
    [alertController addAction:allFilterAction];
    [alertController addAction:llFilterAction];
    [alertController addAction:atFilterAction];
    [alertController addAction:wyFilterAction];
    [alertController addAction:goFilterAction];
    [alertController addAction:inFilterAction];
    [alertController addAction:baFilterAction];
    [alertController addAction:kruxFilterAction];
    
    alertController.popoverPresentationController.sourceView = self.filterButton;
    alertController.popoverPresentationController.sourceRect = self.filterButton.frame;
    
    [self presentViewController:alertController animated:YES completion:nil];
}

// ---------------------------------------------------------------------------------

- (void)__filterWithService:(LADAnalyticsService)service
{
    if (LADAnalyticsAllService == service)
    {
        self.filterEnabled = NO;
    }
    else
    {
        self.filterEnabled = YES;
        NSString *filterServiceName = LADAnalyticsNameFromService(service);
        self.filteredServicesInfosStack = [NSMutableArray new];
        
        for (LADAnalyticsServiceInfos *serviceInfos in self.servicesInfosStack)
        {
            if ([filterServiceName isEqualToString:serviceInfos.serviceName])
            {
                [self.filteredServicesInfosStack addObject:serviceInfos];
            }
        }
    }
    
    [self __refreshAction];
}

// ---------------------------------------------------------------------------------

- (IBAction)__configurationAction:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Configurations Ad"
                                                                             message:[self adConfigurationsCSVString]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alertController addAction:defaultAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

// ---------------------------------------------------------------------------------
#pragma mark - Utils

- (BOOL)__saveCSFFiles
{
    NSData *servicesCSV         = [CSVManager CSVFromString:[self servicesCSVString]];
    NSData *adConfigurationsCSV = [CSVManager CSVFromString:[self adConfigurationsCSVString]];
    
    return ([CSVManager saveCSV:servicesCSV atPath:[self servicesPath]] &&
            [CSVManager saveCSV:adConfigurationsCSV atPath:[self adConfigurationsPath]]);
}

// ---------------------------------------------------------------------------------

- (void)__removeCSVFiles
{
    [CSVManager removeCSVAtPath:[self servicesPath]];
    [CSVManager removeCSVAtPath:[self adConfigurationsPath]];
}

// ---------------------------------------------------------------------------------

@end
