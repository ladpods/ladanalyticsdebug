#import "LADAnalyticsDebug.h"

// Controller
#import "LADAnalyticsDebugDetailsViewController.h"

// View
#import "LADAnalyticsDebugPopup.h"

// ---------------------------------------------------------------------------------
#pragma mark - Constants

#define LADAnalyticsDebugServiceAssert(__SERVICE__) NSAssert(LADAnalyticsUnknownService != __SERVICE__, @"The service(s) must not be LADAnalyticsUnknownService !")


// ---------------------------------------------------------------------------------
#pragma mark - Static

static NSString * const kAnalyticsDebugDeeplink = @"analyticsdebug";
static NSString * const kAnalyticsDebugEnable   = @"enable";
static NSString * const kAnalyticsDebugDisable  = @"disable";

static BOOL _isStarted                          = NO;
static NSTimeInterval _decrementIntervalValue   = 3.;
static LADAnalyticsDebugPopup *_popup           = nil;

static NSMutableArray *_serviceInfosStack       = nil;    // Contains the LADAnalyticsServiceInfos objects.
static NSMutableArray *_adConfigurations        = nil;     // Contains the LADAnalyticsAdConfigurationInfos objects.

static NSLayoutConstraint *_topConstraint       = nil;

// ---------------------------------------------------------------------------------
#pragma mark - Helper

NSString * LADAnalyticsShortNameFromService(LADAnalyticsService service)
{
    NSString *shortName = @"";
    
    if (LADAnalyticsLocalyticsService == service) {
        shortName = @"LL";
    }
    else if (LADAnalyticsATInternetService == service) {
        shortName = @"AT";
    }
    else if (LADAnalyticsWysistatService == service) {
        shortName = @"WY";
    }
    else if (LADAnalyticsGoogleService == service) {
        shortName = @"GA";
    }
    else if (LADAnalyticsAdInterService == service) {
        shortName = @"IN";
    }
    else if (LADAnalyticsAdBannerService == service) {
        shortName = @"BA";
    }
    else if (LADAnalyticsKruxService == service) {
        shortName = @"KRX";
    }
    
    return shortName;
};

// ---------------------------------------------------------------------------------

NSString * LADAnalyticsNameFromService(LADAnalyticsService service)
{
    NSString *name = @"";
    
    if (LADAnalyticsLocalyticsService == service) {
        name = @"Localytics";
    }
    else if (LADAnalyticsATInternetService == service) {
        name = @"ATInternet";
    }
    else if (LADAnalyticsWysistatService == service) {
        name = @"Wysistat";
    }
    else if (LADAnalyticsGoogleService == service) {
        name = @"GoogleAnalytics";
    }
    else if (LADAnalyticsAdInterService == service) {
        name = @"AdInter";
    }
    else if (LADAnalyticsAdBannerService == service) {
        name = @"AdBanner";
    }
    else if (LADAnalyticsKruxService == service) {
        name = @"Krux";
    }
    
    return name;
};

// ---------------------------------------------------------------------------------
#pragma mark - LADAnalyticsServiceInfos


@implementation LADAnalyticsServiceInfos

// ---------------------------------------------------------------------------------

+ (instancetype)infosWithService:(LADAnalyticsService)service
                      complement:(NSString *)complement
{
    
    return [[self alloc] initWithService:service
                              complement:complement];
}

// ---------------------------------------------------------------------------------

- (instancetype)initWithService:(LADAnalyticsService)service
                     complement:(NSString *)complement
{
    self = [super init];
    
    if (self)
    {
        LADAnalyticsDebugServiceAssert(service);
        
        _service = service;
        
        _serviceName = LADAnalyticsNameFromService(service);
        _complement = complement;
        _date = [NSDate date];
    }
    
    return self;
}

// ---------------------------------------------------------------------------------

@end

// ---------------------------------------------------------------------------------
#pragma mark - LADAnalyticsAdConfigurationInfos


@implementation LADAnalyticsAdConfigurationInfos

// ---------------------------------------------------------------------------------

+ (instancetype)configurationInfosWithAdName:(NSString *)adName
                                adIdentifier:(NSString *)adIdentifier
{
    return [[self alloc] initWithAdName:adName
                           adIdentifier:adIdentifier];
}

// ---------------------------------------------------------------------------------

- (instancetype)initWithAdName:(NSString *)adName
                  adIdentifier:(NSString *)adIdentifier
{
    self = [super init];
    
    if (self)
    {
        _adName = adName;
        _adIdentifier = adIdentifier;
    }
    
    return self;
}

// ---------------------------------------------------------------------------------

@end

// ---------------------------------------------------------------------------------
#pragma mark - LADAnalyticsDebug


@implementation LADAnalyticsDebug

// ---------------------------------------------------------------------------------
#pragma mark - Public

+ (BOOL)isAnalyticsDebugDeeplink:(NSString *)deeplink
{
    if ([deeplink containsString:kAnalyticsDebugDeeplink])
    {
        return YES;
    }
    
    return NO;
}

// ---------------------------------------------------------------------------------

+ (void)handleDeeplink:(NSString *)deeplink
      enableCompletion:(void (^)())enableCompletion
     disableCompletion:(void (^)())disableCompletion
{
    if (![self isAnalyticsDebugDeeplink:deeplink])
    {
        return;
    }
    
    if ([deeplink containsString:kAnalyticsDebugEnable])
    {
        NSLog( @"LADAnalyticsDebug version : %@",kLADAnalyticsDebugVersion);
        
        if (enableCompletion)
        {
            enableCompletion();
        }
    }
    
    if ([deeplink containsString:kAnalyticsDebugDisable])
    {
        if (disableCompletion)
        {
            disableCompletion();
        }
    }
}

// ---------------------------------------------------------------------------------

+ (BOOL)isActive
{
    return _isStarted;
}

// ---------------------------------------------------------------------------------

+ (void)startWithServices:(LADAnalyticsService)services
{
    if (_popup)
    {
        return;
    }
    
    LADAnalyticsDebugServiceAssert(services);
    
    _isStarted = YES;
    
    [self initializePopupWithServices:services];
}

// ---------------------------------------------------------------------------------

+ (void)stop
{
    if (!_isStarted ||
        !_popup) {
        
        return;
    }
    
    _isStarted = NO;
    
    [self purge];
}

// ---------------------------------------------------------------------------------

+ (void)incrementServiceWithInfos:(LADAnalyticsServiceInfos *)infos
{
    if (!_isStarted ||
        !infos) {
        
        return;
    }
    
    LADAnalyticsDebugServiceAssert(infos.service);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        // check widget layout
        [self updateWidgetDisplayWithServices:LADAnalyticsUnknownService];
        
        // Increment popup
        
        [_popup incrementService:infos.service];
        [_popup setNeedsDisplay];
        
        // Add infos to stack
        if (!_serviceInfosStack)
        {
            _serviceInfosStack = [NSMutableArray new];
        }
        
        [_serviceInfosStack addObject:infos];
        
    });
}

// ---------------------------------------------------------------------------------

+ (void)setDecrementIntervalValue:(NSTimeInterval)decrementIntervalValue
{
    if (decrementIntervalValue == _decrementIntervalValue)
    {
        return;
    }
    
    _decrementIntervalValue = decrementIntervalValue;
}

// ---------------------------------------------------------------------------------

+ (void)addAdConfigurations:(NSArray<LADAnalyticsAdConfigurationInfos *> *)adConfigurations
{
    if (!_adConfigurations)
    {
        _adConfigurations = [NSMutableArray new];
    }
    
    [_adConfigurations addObjectsFromArray:adConfigurations];
}

// ---------------------------------------------------------------------------------
#pragma mark - Properties

+ (UIWindow *)rootWindow
{
    UIWindow *rootWindow;
    
    if ([[UIApplication sharedApplication].delegate respondsToSelector:@selector(window)])
    {
        rootWindow = [[UIApplication sharedApplication].delegate window];
    }
    
    return rootWindow;
}

// ---------------------------------------------------------------------------------

+ (UIViewController *)rootController
{
    UIViewController *rootController = [self rootWindow].rootViewController;
    
    if (rootController.presentedViewController)
    {
        rootController = rootController.presentedViewController;
    }
    
    return rootController;
}

// ---------------------------------------------------------------------------------

+ (NSUInteger)totalServicesCount
{
    return 7;
}

// ---------------------------------------------------------------------------------

+ (CGFloat)maximumPopupWidth
{
    return 240.;
}

// ---------------------------------------------------------------------------------

+ (CGFloat)horizontalPadding
{
    return 5.;
}

// ---------------------------------------------------------------------------------

+ (NSArray *)popupConstraintsWithServices:(LADAnalyticsService)services
{
    UIWindow *mainWindow = [self rootWindow];
    CGFloat width = ([self servicesCountFromServices:services] * [self maximumPopupWidth]) / [self totalServicesCount];
    
    NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:_popup
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1
                                                                        constant:width];
    
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:_popup
                                                                        attribute:NSLayoutAttributeHeight
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:nil
                                                                        attribute:NSLayoutAttributeNotAnAttribute
                                                                       multiplier:1
                                                                         constant:50];
    
    NSLayoutConstraint *trailingConstraint = [NSLayoutConstraint constraintWithItem:_popup
                                                                          attribute:NSLayoutAttributeRight
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:mainWindow
                                                                          attribute:NSLayoutAttributeRight
                                                                         multiplier:1
                                                                           constant:-[self horizontalPadding]];
    
    _topConstraint = [NSLayoutConstraint constraintWithItem:_popup
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:mainWindow
                                                  attribute:NSLayoutAttributeTop
                                                 multiplier:1
                                                   constant:(CGRectGetHeight(mainWindow.bounds)/2.0) - heightConstraint.constant - [self horizontalPadding]];
    
    return @[widthConstraint, heightConstraint, trailingConstraint, _topConstraint];
}

// ---------------------------------------------------------------------------------
#pragma mark - Popup

+ (void)initializePopupWithServices:(LADAnalyticsService)services
{
    _popup = [LADAnalyticsDebugPopup popupWithServices:services];
    _popup.translatesAutoresizingMaskIntoConstraints = NO;
    _popup.decrementInterval = _decrementIntervalValue;
    
    _popup.onTouchAction = ^{
        [_popup hide];
        
        LADAnalyticsDebugDetailsViewController *detailsViewController = [LADAnalyticsDebugDetailsViewController detailsWithServicesInfosStack:_serviceInfosStack
                                                                                                                             adConfigurations:_adConfigurations];
        detailsViewController.popup = _popup;
        
        [[self rootController] presentViewController:[[UINavigationController alloc] initWithRootViewController:detailsViewController]
                                            animated:YES
                                          completion:nil];
    };
    
    [_popup addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragging:)]];
    
    [self updateWidgetDisplayWithServices:services];
}

// ---------------------------------------------------------------------------------

+ (void)updateWidgetDisplayWithServices:(LADAnalyticsService)services
{
    if (_popup != nil)
    {
        UIWindow *mainWindow = [self rootWindow];
        
        NSAssert(mainWindow != nil, @"Root window must be ready before starting LADAnalyticsDebug");
        
        if (_popup.superview != mainWindow)
        {
            //NSLog(@"Widget Superview id different from detected mainWindow => update display !!!!");
            //NSLog(@"popup superview : %@ | subview: %@",[_popup.superview description], [_popup.superview subviews] );
            
            [mainWindow insertSubview:_popup atIndex:[mainWindow.subviews count]];
            
            if (services != LADAnalyticsUnknownService)
            {
                [mainWindow addConstraints:[self popupConstraintsWithServices:services]];
            }
        }
    }
}

// ---------------------------------------------------------------------------------
#pragma mark - Utils

+ (void)purge
{
    [_popup removeFromSuperview];
    _popup = nil;
    
    [_serviceInfosStack removeAllObjects];
    _serviceInfosStack = nil;
    
    [_adConfigurations removeAllObjects];
    _adConfigurations = nil;
    
    _topConstraint = nil;
}

// ---------------------------------------------------------------------------------

+ (NSUInteger)servicesCountFromServices:(LADAnalyticsService)services
{
    if (LADAnalyticsUnknownService == services)
    {
        return 0;
    }
    
    if (LADAnalyticsAllService == services)
    {
        return [self servicesCountFromServices:(LADAnalyticsLocalyticsService|LADAnalyticsATInternetService|LADAnalyticsWysistatService|LADAnalyticsGoogleService|LADAnalyticsAdInterService|LADAnalyticsAdBannerService | LADAnalyticsKruxService)];
        
    }
    else
    {
        NSUInteger count = 0;
        
        if (services & LADAnalyticsLocalyticsService) {
            count++;
        }
        
        if (services & LADAnalyticsATInternetService) {
            count++;
        }
        
        if (services & LADAnalyticsWysistatService) {
            count++;
        }
        
        if (services & LADAnalyticsGoogleService) {
            count++;
        }
        
        if (services & LADAnalyticsAdInterService) {
            count++;
        }
        
        if (services & LADAnalyticsAdBannerService) {
            count++;
        }
        
        if (services & LADAnalyticsKruxService) {
            count++;
        }
        
        return count;
    }
}

// ---------------------------------------------------------------------------------

static CGFloat beginDragY = 0.;

+ (void)dragging:(UIPanGestureRecognizer *)gesture
{
    UIGestureRecognizerState state = gesture.state;
    CGPoint translation            = [gesture translationInView:[self rootWindow]];
    
    if (UIGestureRecognizerStateBegan == state)
    {
        beginDragY = translation.y;
    }
    else if (UIGestureRecognizerStateChanged == state)
    {
        _topConstraint.constant += translation.y - beginDragY;
        
        beginDragY = translation.y;
        
        [[self rootWindow] setNeedsLayout];
        [[self rootWindow] layoutIfNeeded];
    }
    else if (UIGestureRecognizerStateEnded == state)
    {
        if (_topConstraint.constant < CGRectGetHeight(_popup.bounds))
        {
            _topConstraint.constant = CGRectGetHeight(_popup.bounds) + [self horizontalPadding];
            [[self rootWindow] setNeedsLayout];
            
            [UIView animateWithDuration:.25
                             animations:^{
                                 [[self rootWindow] layoutIfNeeded];
                             }];
        }
        else if ((_topConstraint.constant + CGRectGetHeight(_popup.bounds)) > CGRectGetHeight([self rootWindow].bounds))
        {
            _topConstraint.constant = CGRectGetHeight([self rootWindow].bounds) - CGRectGetHeight(_popup.bounds) - [self horizontalPadding];
            [[self rootWindow] setNeedsLayout];
            
            [UIView animateWithDuration:.25
                             animations:^{
                                 [[self rootWindow] layoutIfNeeded];
                             }];
        }
    }
}

// ---------------------------------------------------------------------------------

@end
