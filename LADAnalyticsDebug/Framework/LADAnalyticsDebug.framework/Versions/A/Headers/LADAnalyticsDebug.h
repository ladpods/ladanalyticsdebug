// LADAnalyticsDebug framework v 1.0.7

#import <Foundation/Foundation.h>

static NSString     *kLADAnalyticsDebugVersion = @"1.0.7 build 42";


// ---------------------------------------------------------------------------------

/*!
 *  The different analytics services.
 */

typedef NS_OPTIONS(NSUInteger, LADAnalyticsService) {
    LADAnalyticsUnknownService      = 0,
    
    LADAnalyticsLocalyticsService   = 1 << 0,   // Localytics
    LADAnalyticsATInternetService   = 1 << 1,   // ATInternet
    LADAnalyticsWysistatService     = 1 << 2,   // Wysistat
    LADAnalyticsGoogleService       = 1 << 3,   // Google analytics
    LADAnalyticsAdInterService      = 1 << 4,   // Ad inter
    LADAnalyticsAdBannerService     = 1 << 5,   // Ad banner
    LADAnalyticsKruxService         = 1 << 6,   // Krux Analytics
    
    LADAnalyticsAllService          = 1 << 7
};

// ---------------------------------------------------------------------------------

/*!
 *  Helper to get the name & short name from an analytics service
 */


NSString * LADAnalyticsShortNameFromService(LADAnalyticsService service);
NSString * LADAnalyticsNameFromService(LADAnalyticsService service);

// ---------------------------------------------------------------------------------
#pragma mark - LADAnalyticsServiceInfos


@interface LADAnalyticsServiceInfos : NSObject


/*!
 * @discussion Initialize an analytics infos object. Use in the details controller to display all the added LADAnalyticsServiceInfos.
 * @param service The analytics service to add.
 * @param complement A complement string that contains different infos about the current analytics service to add.
 */

+ (instancetype)infosWithService:(LADAnalyticsService)service
                      complement:(NSString *)complement;

@property (nonatomic, assign, readonly) LADAnalyticsService service;    // The analytics service.

@property (nonatomic, copy, readonly) NSString *serviceName;            // The analytics service name.
@property (nonatomic, copy, readonly) NSString *complement;             // The analytics service complement infos.
@property (nonatomic, strong, readonly) NSDate *date;                   // The analytics service date. [NSDate date] when LADAnalyticsInfos is added.


@end

// ---------------------------------------------------------------------------------
#pragma mark - LADAnalyticsAdConfigurationInfos


@interface LADAnalyticsAdConfigurationInfos : NSObject


/*!
 * @discussion Initialize an an configuration infos object. Use in the details controller to display ad configurations.
 * @param adName The ad name/key.
 * @param adIdentifier The ad identifier.
 */

+ (instancetype)configurationInfosWithAdName:(NSString *)adName
                                adIdentifier:(NSString *)adIdentifier;

@property (nonatomic, copy, readonly) NSString *adName;
@property (nonatomic, copy, readonly) NSString *adIdentifier;


@end

// ---------------------------------------------------------------------------------
#pragma mark - LADAnalyticsDebug


@interface LADAnalyticsDebug : NSObject


/*!
 * @discussion Tests if the deeplink contains 'analyticsdebug' (appname://analyticsdebug/...).
 * @param deeplink The deeplink.
 */
+ (BOOL)isAnalyticsDebugDeeplink:(NSString *)deeplink;


/*!
 * @discussion Handle the analytics debug deeplink.
 * @param deeplink The deeplink.
 * @param enableCompletion The completion block to be executed if 'deeplink' contains 'enable' (appname://analyticsdebug/enable).
 * @param disableCompletion The completion block to be executed if 'deeplink' contains 'disable' (appname://analyticsdebug/disable).
 */
+ (void)handleDeeplink:(NSString *)deeplink
      enableCompletion:(void (^) ())enableCompletion
     disableCompletion:(void (^) ())disableCompletion;

/*!
 * @discussion Know if the service is started
 */
+ (BOOL)isActive;

/*!
 * @discussion Start the analytics debug. It will present a debug popup with a counter for all choosen analytics services.
 * @param services The analytics services to debug.
 */
+ (void)startWithServices:(LADAnalyticsService)services;


/*!
 * @discussion Stop the analytics debug. It will remove the current debug popup.
 */
+ (void)stop;


/*!
 * @discussion Increment a counter for choosen analytics services and add a LADAnalyticsServiceInfos object to the current infos stack to display complement details. It will decrement after 3s by default or decrementIntervalValue if set.
 * @param serviceInfos The service infos object to add.
 */
+ (void)incrementServiceWithInfos:(LADAnalyticsServiceInfos *)infos;


/*!
 * @discussion Replace the default decrement interval value.
 * @param decrementIntervalValue The choosen decrement interval value.
 */
+ (void)setDecrementIntervalValue:(NSTimeInterval)decrementIntervalValue;


/*!
 * @discussion Add app ad configurations (Inter, banner, DFP, ...).
 * @param adConfigurations Contains the ad configuration to add.
 */
+ (void)addAdConfigurations:(NSArray<LADAnalyticsAdConfigurationInfos *> *)adConfigurations;

// test 
@end
