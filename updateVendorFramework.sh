#!/bin/bash
echo "Start updating LADAnalyticsDebug Framework"

# Get path to package directory
packageDirectory=$(ls | grep LADAnalyticsDebug-)
stepCounter=1

echo $packageDirectory

if [ -n "$packageDirectory" ]; then
    echo "Step $stepCounter - Delete old package directory"
	rm -rf $packageDirectory
	((stepCounter++))
fi

echo "Step $stepCounter - Create new package directory"
pod package LADAnalyticsDebug.podspec

((stepCounter++))

#get path to package directory
packageDirectory=$(ls | grep LADAnalyticsDebug-)

echo $packageDirectory 

echo "Step $stepCounter - Delete old vendor framework"
rm -rf ./LADAnalyticsDebug/Framework/LADAnalyticsDebug.framework

((stepCounter++))

echo "Step $stepCounter - move new packaged framework to vendor directory"
mv $packageDirectory/ios/LADAnalyticsDebug.framework ./LADAnalyticsDebug/Framework/

((stepCounter++))

echo "Step $stepCounter - Delete package directory"
rm -rf $packageDirectory

echo "Finish"