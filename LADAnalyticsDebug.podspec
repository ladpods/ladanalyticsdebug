Pod::Spec.new do |s|
    s.name             = 'LADAnalyticsDebug'
    s.version          = '1.0.9'
    s.summary          = 'iOS Framework for track and debug some analytics services call inside apps (GoogleAnalytics, Localytics, XiTi,...)'

    s.homepage         = 'https://gitlab.com/ladpods/ladanalyticsdebug'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'laddev' => 'jean-baptiste.castro@lagardere-active.com','ydupuy'=>'yann.dupuy@lagardere-active.com' }
    s.source           = { :git => 'https://gitlab.com/ladpods/ladanalyticsdebug.git', :tag => s.version.to_s }

    s.ios.deployment_target = '8.0'

    s.source_files = 'LADAnalyticsDebug/Classes/**/*', 'LADAnalyticsDebug/Shared/*'
    s.public_header_files = 'LADAnalyticsDebug/Shared/LADAnalyticsDebug.h'
    s.private_header_files = 'LADAnalyticsDebug/Classes/**/*.h'

    s.resource_bundles = {
        'LADAnalyticsDebug' => ['LADAnalyticsDebug/Assets/*.xib']
    }
end


